<h2> Table of Contents

[[_TOC_]]

# [TPPSc](https://treegenesdb.org/tppsc) Tutorial Overview

- TPPSc is a 4 page form that serves as the internal pipeline to submit scientific data to our database. This tutorial is meant to show users how to successfully use TPPSc to upload data to your database, along with how to specifically use certain features, and what to do depending on what type of study it is. 
- Our goal is to upload every study we review, but unfortunately that is not possible. There are two major sets of data that are uploaded with each study. Tree accession information and the scientific data that is paired with those trees.
- What do we mean by "Tree accession"? A tree accession file contains the tree ID (or accession number), lat/long cooridantes (WGS 84 in DMS), and the Genus and species of the tree. This location data is then paired with the scientific data such as the recorded phenotypes of the trees, recorded SNPs, or even environmental data.
- In order for a study to be uploaded, both the tree accession file and scientific data file need to fufill the requirments mentioned above. If a study only has one of the two files up to standards, an email is sent to the first author and corresponding author requesting further information on their studies.
- Once a study is successfully uploaded and viewable on CartograTree, an email is sent to the first author and corresponding author, to let them know their data has been uploaded, to invite them to submit to Treegenes in the future, and to provide them with a URL to their pubically viewable data.
- Before we go through how to specifically upload a study, I will review what to do to curate and find new studies.
- Big note on TPPSc. It is a user created module and will occasionally run into an error or bug here and there. The first thing to do when you run into an error that you think is unrelated to your files: refresh the page and click save on the bottom. If that doesnt fix the error, promptly let the curation team now (myself or peter) and we will be on a fix as soon as possible!



<h4> Finding new Studies applicable for Curation

One of the primary locations we use to find studies that are more likely to have good data is Dryad since it is a hosing site for supplemental files. However, we can start anywhere and also might find most of what we need as files hosted through the journal where an article is published. https://datadryad.org/search. Upon searching the dryad database, use terms such as "population genetics, plants ssr, plants snps, etc" to attempt and find new studies that may be applicable for submission. If you think you have found a study that is applicable, first search (control-F) the dryad DOI against [this spreadsheet](https://docs.google.com/spreadsheets/d/1tJNNztxGhgZn-moM6N6NjO5R8klsEzEi5bEvBrAhRZs/edit?resourcekey#gid=1786026535) to ensure we have not uploaded it so far. An example of the drayd DOI: datadryad.org/resource/doi:10.5061/dryad.**5jn04** with the bolded being what you search.

<h1> Creating a New TPPSc Submission

Here you have the opportunity to create a new TPPSc Submission and the chance to continue working on any unfinished submissions. Simply use the dropdown to select your option and proceed to the next page. 

<img src="/screenshots/tppscCreateNewSub.PNG" width="480">

<h3> Author and Species Information (Page 1)

This page requires you to enter a dryad DOI accession number of the study you plan on uploading. The dryad DOI should pull the species, authors, abstract, title, and journal of the paper. This allows the user to enter only one textfield to pull all relevant information. 

Note, you do not enter the DOI, but the dryad DOI, and not the entire link, but only past the colon. If the full dryad DOI link was https://datadryad.org/resource/doi:10.5061/dryad.5jn04, then only enter 10.5061/dryad.5jn04.

<img src="/screenshots/tppscA_Snoinfo.PNG" width="400"> <img src="/screenshots/tppscA_Sinfo.PNG" width="360"> <img src="/screenshots/RHBafterA_S.PNG" width="150">

To check if it successfully pulled all relevant information, theres a green right hand bar that updates as you move throughout the form. Above is an example of how it should look once you move to the next page.

<h3> Experimental Conditions (Page 2)

This page consists of two dropdowns that directly affects pages 3 and 4. The Data Type dropdown is where you chose what type of scientific data you are uploading, whether it be Genotype, Phenotype, Environment, or any combination of the 3. The Study Type dropdown is where you chose what kind of study you are uploading. The options for this field are Natural Population, Growth Chamber, Greenhouse, Experimental/Common Garden, and Plantation. The Data Type selection directly affects page 4 and the Study Type selection directly affects page 3.

<img src="/screenshots/tppscEI.PNG" width="400">

<h3> Tree Accession (Page 3)

This page is where we upload the tree location information. We do this by uploading an excel file that has all of the location information. Once the file is uploaded, you then select which columns represent which required data, where the required data is TreeID, Latitude, and Longitude (and if there are multiple species, then a column that represents the genus and species). Also be sure to select whether the tree location information is exact or approximate, and if approxamite then put in the correct approximation information. Approximate would be considered anything centriod or when you only have population location information and not exact tree location. An example of a well curated Tree Accession file is linked [here](https://treegenesdb.org/sites/default/files/tpps_accession/TGDR001_Tree_Accession_Populus_trichocarpa.csv).

<img src="/screenshots/tppscTreeAccnoinfo.PNG" width="350"> <img src="/screenshots/treeACCdata.PNG" width="420"> <img src="/screenshots/treeACCsample.PNG" width="150">

<h3> Phenotype, Genotype, and Environmental Data (Page 4)

Page 4 will change depending on what you selected for the Data Type dropdown. If you selected an option with Genotype, then the Genotype fields will be available. The same goes for phenotype and Environment.

<h4> Phenotype

When dealing with recorded phenotypes, we create phenotypes in tppsc, and then upload a phenotype file which uses corressponding phenotypes. Each phenotype has 5 different values: Name (human readable name, must match column name exactly), Attribute (thing the phenotype is describing: age, height, amount, etc), Description, Structure (entity, describing what part of the plant is being measure), and Units. To upload your phenotypic data, simply click add/remove phenotype until the correct number of phenotypes are created, and then fill out the corresponding metadata for each phenotype. Select from the dropdown menus for Attribute, Structure, and units as this is to ensure that all phenotypes use these sets of specific terms. 

When uploading over 20+ phenotypes: a user can enter them all by creating an excel file with those 5 columns (name, attribute, description, structure, and units). NOTE: when uploading a phenotype metadata file, be sure to use the dropdown boxes for attribute, units, and structure to ensure that you are using the correct database readable terms so that the data uploaded can be used in conjuction with other realted phenotype data. After creating each phenotype, you now need to upload the file. There are two types of file formats that the phenotype file can be in. File type 1 has all the phenotypes in their own columns, while file type 2 has 3 total columns: TreeID, phenotype, and value. Either are accepted, and once uploaded the user will need to chose which columns represent what, just as done previously. An example of a well curated phenotype file is [here](https://treegenesdb.org/sites/default/files/tpps_phenotype/TGDR001_Phenotype_Data_Populus_trichocarpa.xlsx).

<img src="/screenshots/PhenotypeINFO.png" width="450"> <img src="/screenshots/phenotypeFILE.PNG" width="450"> 

<h4> Genotype

The main two types of genotypic data that TPPSc handles is SNP data, and SSR data. The first option in the genotype section requires you to declare which type of genotypic data will be uploaded. For both situations, there is a dropdown field that asks if a reference genome is used.

**SNP:** When dealing with a SNP study, first you select what kind of SNP's you have by selecting which kind of experimental design it is (based on the article). The possible dropdowns for Experimental Design are GBS, Targeted Capture, Whole Genome Resequencing, RNA-Seq, and Genotyping Array.

*  IF GBS is selected, another dropdown for GBS Type appears with options RADSeq, ddRAD-Seq, NextRAD, and RAPTURE
*  IF Targeted Capture is selected, another dropdown for Targeted Capture Type appears with options of Exome Capture or Other

SNP data will come in either a .csv, .tsv, or .vcf format. To upload the SNP data, check the SNPs Genotype Assay box for .csv or .tsv files or VCF for .vcf files. If SNPs Genotype Assay is chosen, you will be provided with another set of options; Assay Design and SNP Association. When dealing with Genotype Assays, ideally you should upload the corresponding Assay Design file. This file contains mapping and quality information about the SNP file. Additionally, there is an option for the very important SNP Association file. This file type associated the phenotypic traits and environmental variables to specific SNP locations. An example of each type of SNP file is linked [here](https://treegenesdb.org/sites/default/files/tpps_genotype/TGDR001_Genotype_SNPs_Assay_Populus_trichocarpa.xlsx).


<img src="/screenshots/tppsCSNP.PNG" width="450"> <img src="/screenshots/tppscSNPsampledata.PNG" width="450">

**VCF:** 
<ul>
<li>When uploading a VCF file there are two options: upload the VCF from your local device or provid a path to the UCONN-xanadu cluster. For very large VCF files, sign into the UCONN-xanadu cluster and direct yourself to /core/labs/Wegrzyn/VCF (cd is the command to change directory). Once you are within the VCF directory (you can confirm this by using the pwd command), use the command wget and the pathname of the file to store a file from the internet on the cluster. You can obtain the full path to the file by right-clicking the file link and then clicking Copy Link Address in the dropdown menu. (example: you might write wget https://datadryad.org/stash/downloads/file_stream/16442). Your VCF will now be stored in the cluster. If you wish to rename your file, use the command mv (example: mv old_name.vcf.gz new_name.vcf.gz). It is generally best practice to rename your VCF in a way that corresponds to the study upload to which it will belong (example: for study TGDR001, you might name your VCF 001.vcf.gz).<\li>

<li>Return to TPPSc and paste the full path in the corresponding field. The full path includes everything above followed by the file (examples: /core/labs/Wegrzyn/VCF/populustrich.vcf.gz, or /core/labs/Wegrzyn/VCF/new_name.vcf.gz). Next, click on the Pre-Validate VCF button. This creates a tripal job on the jobs page.<\li>

<li>On the jobs page, click on the Execute button, and the job should be executed immediately.<\li>

<li>Return to TPPSc and click Review Information and Submit, and continue with the submission of the study as usual.</li>
<\ul>

**SNP Assay Design:** These files contain relevant metadata about the SNP files. The typical fields required in this excel sheet would be: scaffold name, position, flank sequence, type or marker, and version of ref genome SNP assay. An example of a well curated Assay Design file is [here](https://treegenesdb.org/sites/default/files/tpps_genotype/snp%20data_0.xlsx).
<ul>
<li>For .vcf files, the metadata is already included within the original file.<\li>
</ul>

**SNP Association**: One of our main priorities is to upload SNP Association information to our database. In short, this is an additional file that confidently makes associations between SNPs and phenotypes, while giving locations of the SNPs and confidence values as well. An example of the data we are looking for is below. There is a minimum amount of data needed for this file to be useful, and any additional information is denoted with (optional) in the header. 

<img src="/screenshots/association.png" width="450">

**SSR:** When dealing with an SSR study, first you describe what type of SSR/cpSSR in the provided drop down. Another field labeled Ploidy then appears which is very important when uploading the SSR/cpSSR spreadsheet. If an organism is diploid, then there would be two columns for each SSR. If the SSR is of  haploid chloroplast genes, then there would be one column for each SSR. Below is a sample data set for SSR markers where the ploidy would be diploid.

<img src="/screenshots/tppscSSR.PNG" width="450"> <img src="/screenshots/tppscSSRsampledata.PNG" width="450">


<h4> Environment

When dealing with environmental data, there are two types of data. Either select that you used Environmental Layers indexed by CartograTree (Major Soil Types, PET and Aridity, Canopy Height, Land Cover, Intact Forest Landscapes, and Worldclim v.2), or add layers that you collected yourself. When adding layers that you collected yourself, one would add the custom environmental data as a phenotype, and select the "Phenotype # is an environmental phenotype". When selected from layers indexed previously, go through the process of checking what layers you specifically have. When uploading a custom environmental layer that is not loaded on our database, ensure to upload the DB URL, layer name, and the specific parameters used in that layer.

<img src="/screenshots/Environmentalinfo.PNG" width="450"> 

<img src="/screenshots/Indexedlayers.PNG" width="450"> 

<h1> Review Page and Post Submission

Species Images: If on the review page for TPPSc it states that our database does not have an image for this species, then follow our [species image curation protocol](https://gitlab.com/TreeGenes/data-curation/-/blob/master/Species%20Image%20Curation.md) to find an image (if possible). Name the species genus_species.jpg, and upload the relevant species image information (image + licensing info) in the fields that populate at the end of the tppsc submission!

<img src="/screenshots/speciesimages.png" width="750">

There are a few things to do after submission of the study. First, and most importantly, approve the study into the database. Head to the https://treegenesdb.org/tpps-admin-panel, find your study, click on the TGDR accession, scroll down, check the box "This study has been reviewed and approved" and approve it.

You then need to check the TPPS detail page: https://treegenesdb.org/tpps/details/TGDR001 and the CartograTree page: https://treegenesdb.org/cartogratree?accession=TGDR001 for the new TGDR of your study. Once you have ensured that the TPPS detail page, the cartogra page, and that you have searched for an applicble species image, then tag myself (charlie) and irene on the clickup form of the study to take a look at it, and once we have confirmed everything looks good, then we will let you know to send the emails out to the authors!

Upon confimration that the study looks correct, ensure that you email the first and corresponding author to let them know that their data has been uploaded. Use the [email templates](https://gitlab.com/TreeGenes/data-curation/-/tree/master/EmailTemplates) in this Gitlab to do so.

<h2> Submission Errors and ClickUp

I often find that when uploading a study, I am not able to get through every step, or run into some sort of error along the way. This is a common occurance and we have a way to deal with it, through the treegenes Curation [ClickUp](https://app.clickup.com/2321068/v/l/s/10526986). The Curation clickup has 2 separate lists, Study Loads and Data Curation. Study Loads is where we put in progress and completed studies. To create a study, create a Task, and click then load the TPPSc study template. Then, input the name of the study as the task name, and any important curation details in the description below. It also auto populates a list of subtasks needed before completion of the study (and follows along with the steps from above). You can also assign the task to yourself to designate who is curating it, and assign it to others if you need their assistance Below is a screenshot of a current study we are working on. This is important to do for completed studies (to create records in ClickUp) and for in progress studies as it contains all relevant curation information so that I or someone else can assist with curation. Be sure to also add in any corresponding files/screenshots/ other information, ALONG WITH THE TGDR number in the TGDR field. TGDR number makes the clickup MUCH easier to view.

<img src="/screenshots/clickup.png" width="450">

<h2> Reuploading an already submitted TPPSc study

Say you submit your study and its uploaded and created all successfully but then you notice that you have some sort of error that you missed when curating. That is okay. There is a nice way to fix that! Head over to the https://treegenesdb.org/tpps-admin-panel , scroll down to Change State Status, and change the state to "Incomplete". From there, you will be able to then proceed back to the treegenesdb.org/tppsc/TGDR### page and edit your study accordingly, without any big database errors. Once you are finished editing, submit and approve just as your normally would!
