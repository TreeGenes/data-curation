Detailed below is the pipeline for curation `.fa` and `.fna` files into the TreeGenes database.

1. [Find Genome](#find-genome)
2. [Create Analysis](#create-analysis)
3. [Transfer Genome File to Temp Folder in the Cluster](#transfer-file)
4. [Upload File to Database](#upload-file)
5. [GFF Files](#gff-files)

# <a id="find-genome"></a>Find a Genome

The first step is to find a genome for curation. Useful repositories include NCBI and Phytozome.
[This link](https://docs.google.com/spreadsheets/d/1YocOS5k4vluxOZbfh7jZ0A4-D_3XRHzOBAEbFr1JxTE/edit#gid=1801186117) allows you to view three priority lists of genomes to load.
* **Primary list:** genomes of tree species with associated studies in CartograPlant
* **Secondary list:** genomes from known released studies
* **Tertiary list:** remaining genomes available to load

# <a id="create-analysis"></a>Create Analysis

Now, we will create an analysis.

* **Go to** [this link](https://treegenesdb.org/bio_data/add/2)

* **Name**
    * *If your genome has a specific name:* “<Genus species> genome v<#.#>” (Example: “Populus trichocarpa  genome v2.2”)
    * *If your genome does not have a specific name:*
        * *If this species is not yet represented by a genome in the database:* “<Genus species> genome v1.0" (Example: “Arabidopsis thaliana genome v1.0”)
        * *If this species is already represented by a genome(s) in the database:* “<Genus species> genome v<n + 1>.0", where `n` is the current highest version number of that species' genome available in the database. (Example: Given that the current highest verion of the *Arabidopsis thaliana* genome in the database is v1.0, our name should be “Arabidopsis thaliana genome v2.0”)

* **Description:** copy from the original paper. If your genome is not from a paper, leave this field blank.

* **Program, Pipeline, Workflow, or Method Name:** "<Genus species> genome and assembly v<#.#>" (Example: "Populus trichocarpa genome and assembly v2.2")

* **Program Version:** the version of the genome in question (Example: "v2.2")

* **Data Source Name:** the name of the site where you found the genome (Example: NCBI)

* **Data Source URI:** URI (identifier, not necessarily an address) where the genome can be found

* Hit "Save" when done.

# <a id="transfer-file"></a>Transfer Genome File to Temp Folder in the Cluster

* **Remote tunnel into the cluster** in terminal or third party (FileZilla / Cyberduck). We recommend using SFTP:
    * `sftp <username>@transfer.cam.uchc.edu`
    **OR**
    * Use SCP Command if in normal SSH Tunnel when copying files over (`scp <path/to/local/file>.zip <username>@treegenesdb.org:/isg/treegenes/treegenes_store/FTP/temp/<folder_name>`)
* **Navigate to file path**
    * cd /isg/treegenes/treegenes_store/FTP/temp/<folder_name>
    * Create a temporary folder with the `mkdir` command
    * Copy file(s) into your temporary folder
    * Determine number of scaffolds
        * this is a size/time estimate
        * `grep -c '>' <filename>`)

# <a id="upload-file"></a>Upload File to Database
* Go to [the FASTA loader](https://treegenesdb.org/admin/tripal/loaders/chado_fasta_loader)
* Enter the remote path: https://treegenesdb.org/FTP/temp/<folder_name>/<file_name>
* Select analysis (created in Step 2)
* Select organism (from dropdown)
* Sequence Type: supercontig
* Import FASTA file
* Execute the job on [the jobs page](https://treegenesdb.org/admin/tripal/tripal_jobs), or wait for Tripal Daemon execution

# <a id="gff-files"></a>GFF Files

If there is an associated GFF file, the same steps can be performed using .gff. In Chado, you do not need to specify sequence type.
