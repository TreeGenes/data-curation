#!/bin/bash
#SBATCH --job-name=SNP_probe_remapping
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=15G
#SBATCH --qos=general
#SBATCH --error=SNP_probe_remapping-%j.err

##############################################################  USER INPUT  #######################################################################
# Input parameters
NEW_GENOME="/path/to/new_genome.fa"       # Path to new genome to map to
NEW_GENOME_VERSION="v4"                    # v{integer}
SPECIES="Populus_trichocarpa"              # {Genus}_{species}
VCF_FILE="/path/to/input.vcf"              # Path to input VCF file
ORIGINAL_GENOME="/path/to/original.genome.fa"  # Path to the original genome used to create the VCF

# Output file settings
OUTPUT_FILE="${SPECIES}.${NEW_GENOME_VERSION}.remap.csv"
FLANK_SIZE=30                              # Size of flanking sequence to extract around each SNP

####################################################################################################################################################

# Basic error handling setup
set -e              # Exit on any error
set -o pipefail     # Pipe operations fail on any component failure

# Set number of threads for parallel processing
THREADS=36

# Load required modules
module load bwa
module load samtools
module load bcftools
module load bedtools

# Set up temporary directory - prefer RAM disk if available
if [[ -d "/dev/shm" && -w "/dev/shm" ]]; then
    TMPDIR="/dev/shm/$USER/snp_remap_$$"    # Use RAM disk for better performance
else
    TMPDIR="/tmp/$USER/snp_remap_$$"        # Fallback to regular tmp
fi
mkdir -p "$TMPDIR"

# Define temporary file paths
TEMP_BAM="$TMPDIR/aligned_snps.bam"
TEMP_SORTED_BAM="$TMPDIR/aligned_snps_sorted.bam"
TEMP_BED="$TMPDIR/aligned_snps.bed"
TEMP_FASTA="$TMPDIR/temp_snps.fasta"
TEMP_REMAPPED="$TMPDIR/temp_remapped.fa"

# Cleanup function to remove temporary files
cleanup() {
    rm -rf "$TMPDIR"
}
trap cleanup EXIT    # Ensure cleanup runs even if script fails

# Helper function to verify input files exist and are readable
check_file() {
    if [[ ! -r "$1" ]]; then
        echo "Error: File $1 does not exist or is not readable." >&2
        exit 1
    fi
}

# Validate input files
check_file "$NEW_GENOME"
check_file "$ORIGINAL_GENOME"
check_file "$VCF_FILE"

# Create genome indices if they don't exist
if [[ ! -f "${NEW_GENOME}.fai" ]]; then
    echo "Indexing reference genome..."
    samtools faidx "$NEW_GENOME"
fi
if [[ ! -f "${NEW_GENOME}.bwt" ]]; then
    echo "Creating BWA index for reference genome..."
    bwa index "$NEW_GENOME"
fi
if [[ ! -f "${ORIGINAL_GENOME}.fai" ]]; then
    echo "Indexing original reference genome..."
    samtools faidx "$ORIGINAL_GENOME"
fi

# Extract SNP sequences and positions from the VCF
echo "Extracting SNP sequences and positions from the VCF..."
bcftools view -H "$VCF_FILE" | \
awk -v FLANK_SIZE="$FLANK_SIZE" -v ORIGINAL_GENOME="$ORIGINAL_GENOME" '
{
    chr=$1;
    pos=$2;
    ref=$4;
    alt=$5;
    name=$3;

    # Extract flanking sequence from the original genome
    start=pos-FLANK_SIZE;
    end=pos+FLANK_SIZE;
    flank=$(samtools faidx ORIGINAL_GENOME "${chr}:${start}-${end}");

    # Replace the SNP position with the alternate allele
    gsub(ref, alt, flank);

    # Print the SNP information
    print ">"name";"ref"/"alt, flank;
}' > "$TEMP_FASTA"

# Align sequences and convert to sorted BAM in a single pipeline
echo "Aligning sequences and converting to sorted BAM..."
bwa mem -t "$THREADS" -K 100000000 -Y "$NEW_GENOME" "$TEMP_FASTA" | \
    samtools view -@ "$THREADS" -bS -q 30 - | \
    samtools sort -@ "$THREADS" -m 2G -T "$TMPDIR/temp_sort" -o "$TEMP_SORTED_BAM"
samtools index -@ "$THREADS" "$TEMP_SORTED_BAM"

# Convert to BED format for easier processing
echo "Converting BAM to BED..."
bedtools bamtobed -i "$TEMP_SORTED_BAM" > "$TEMP_BED"

# Process alignments and create final output
echo "Processing alignments and creating output..."
echo "SNP_name,old_genome_chromosome,old_genome_base_position,new_genome_chromosome,new_genome_base_position,remapping_quality,original_sequence,new_sequence" > "$OUTPUT_FILE"

awk -F'[,\t]' '
BEGIN {
    OFS=",";
    PROCINFO["sorted_in"] = "@ind_num_asc"
}
# Read BED file alignments
{
    name=$4;
    chr=$1;
    pos=$2;
    end=$3;
    strand=$6;
    qual=$5;

    split(name, parts, ";");
    snp_name=parts[1];
    original_ref=parts[2];
    original_alt=parts[3];

    if (strand == "+") {
        new_pos=pos + FLANK_SIZE + 1;
    } else {
        new_pos=end - FLANK_SIZE - 1;
    }

    print snp_name, parts[1], parts[2], chr, new_pos, qual, original_ref "/" original_alt, parts[4];
}
' "$TEMP_BED" >> "$OUTPUT_FILE"

echo "Processing complete. Results saved in $OUTPUT_FILE."

# Clean up error files if everything worked
if [ $? -eq 0 ]; then
    rm "SNP_probe_remapping-${SLURM_JOB_ID}.err"
    rm "slurm-${SLURM_JOB_ID}.out"
fi