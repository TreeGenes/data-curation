#################################################################### USER INPUT ###########################################################
# INPUT NEEDED FOR ALL OPTIONS
fasta_path = "/Users/meghanmyles/Desktop/Populus_trichocarpa/genomes/v4/Ptrichocarpa_533_v4.0.fa"    # path to fasta for vcf
assay_path = "/Users/meghanmyles/Desktop/Populus_trichocarpa/studies/Geraldes_2013/Geraldes_2013.SNP_assay.csv"
assay_design_path = "/Users/meghanmyles/Desktop/Populus_trichocarpa/studies/Geraldes_2013/Geraldes_2013.SNP_assay_design.csv"

fasta_online_path = ""   # should link to our FTP site (correct directory)
species = "Populus trichocarpa"
study_info = "Geraldes_2013"    # FirstAuthor_year
assembly_version = "v4"

# colname assignments (from a table with dropdown headers in TPPS) (0-indexed)
assay_design_snp_name_col = 0
assay_design_snp_chrom_col = 1      # ONLY AN INT IF THE ASSAY DESIGN GIVES CHROM, ELSE None
assay_design_snp_base_pos_col = 2   # ONLY AN INT IF THE ASSAY DESIGN GIVES POS, ELSE None

assay_design_ref_allele_col = None  # optional (None IF NOT INCLUDED)
assay_design_alt_allele_col = None  # optional (None IF NOT INCLUDED)

assay_design_qual_col = 11  # optional (None IF NOT INCLUDED)

# FILE NEEDED ONLY ASSAY DESIGN DOES NOT GIVE CHROM AND POS, ELSE ""
SNP_remapping_path = "/Users/meghanmyles/Desktop/Populus_trichocarpa/bwa_remapped_csvs/Geraldes_2013.FlanksMappedTo.Populus_trichocarpa.v4.csv"
############################################################################################################################################

#############################################################  OUTPUT  #####################################################################
# a VCF of the SNPs on their original genome
############################################################################################################################################


# LIBRARIES
import pandas as pd
import numpy as np
from Bio import SeqIO
from io import StringIO
from datetime import datetime
import hashlib
import re
import os

assay = pd.read_csv(assay_path, low_memory = False)
assay_design = pd.read_csv(assay_design_path, low_memory = False)

if SNP_remapping_path != None:
    remapping = pd.read_csv(SNP_remapping_path, low_memory = False)
    remapping = remapping[remapping['new_genome_chromosome'].notna() & (remapping['new_genome_chromosome'].str.strip() != '')]

# preprocess assay to standardize
def standardize_genotype_vectorized(series):
    """
    Vectorized standardization of genotype values for a pandas Series.
    
    Parameters:
    series (pandas.Series): Genotype values to standardize
    
    Returns:
    pandas.Series: Standardized genotype values
    """
    # Handle missing or invalid values upfront
    standardized = series.fillna('..').astype(str).str.strip().str.upper()
    standardized = standardized.replace(['', '--'], '..')

    # Extract only valid bases (A, C, T, G)
    bases_extracted = standardized.str.findall(r'[ACTG]')
    
    # Define lambda functions for mapping results
    def standardize_bases(bases):
        if len(bases) == 0:
            return '..'
        if len(bases) == 1:
            return f"{bases[0]}."
        return ''.join(sorted(bases[:2]))  # Ensure consistent ordering and limit to 2 bases
    
    # Apply standardization logic
    return bases_extracted.map(standardize_bases)

def preprocess_genotype_data_vectorized(df):
    """
    Preprocesses a DataFrame of genotype data by standardizing all values
    
    Parameters:
    df (pandas.DataFrame): Input genotype data
    
    Returns:
    pandas.DataFrame: Preprocessed data with standardized genotypes
    """
    # Create a copy to avoid modifying the original
    processed_df = df.copy()
    
    # Apply vectorized standardization to all columns except 'SNP'
    for col in processed_df.columns:
        if col != 'SNP':
            processed_df[col] = standardize_genotype_vectorized(processed_df[col])
    
    return processed_df

# Apply the optimized function
assay = preprocess_genotype_data_vectorized(assay)

# CREATE MOCK VCF DATAFRAME WITH SNP NAME + POSITION
len_assay_design = len(assay_design)

VCF_table = pd.DataFrame()

if assay_design_snp_chrom_col == None:
    VCF_table['CHROM'] = ['.'] * len_assay_design
else:
    VCF_table['CHROM'] = assay_design.iloc[:, assay_design_snp_chrom_col]

if assay_design_snp_base_pos_col == None:
    VCF_table['POS'] = ['.'] * len_assay_design
else:
    VCF_table['POS'] = assay_design.iloc[:, assay_design_snp_base_pos_col]

VCF_table['ID'] = assay_design.iloc[:, assay_design_snp_name_col]

if assay_design_ref_allele_col == None:
    VCF_table['REF'] = ['.'] * len_assay_design
else:
    VCF_table['REF'] = assay_design.iloc[:, assay_design_ref_allele_col]

if assay_design_alt_allele_col == None:
    VCF_table['ALT'] = ['.'] * len_assay_design
else:
    VCF_table['ALT'] = assay_design.iloc[:, assay_design_alt_allele_col]

if assay_design_qual_col == None:
    VCF_table['QUAL'] = ['.'] * len_assay_design
else:
    VCF_table['QUAL'] = assay_design.iloc[:, assay_design_qual_col]

VCF_table['FILTER'] = ['.'] * len_assay_design
VCF_table['INFO'] = ['.'] * len_assay_design
VCF_table['FORMAT'] = ['GT'] * len_assay_design

# ADD REFERENCE ALLELES TO THE MOCK VCF
# Read the entire fasta file into memory
genome = SeqIO.to_dict(SeqIO.parse(fasta_path, "fasta"))

# CHECKS TO ENSURE THAT THE MAJORITY OF CHR NAMES REFERENCED WITHIN THE VCF EXIST WITHIN THE FASTA
VCF_table['CHROM'] = VCF_table['CHROM'].fillna('.')

VCF_table['POS'] = pd.to_numeric(VCF_table['POS'], errors='coerce')

# DROP ROWS WITHOUT CHROM OR POS
VCF_table = VCF_table[(VCF_table['CHROM'] != '.') & (VCF_table['POS'] != '.')]

# Update VCF_table with remapping coordinates
def update_vcf_coordinates(vcf_table, remapping):
    # Create a copy to avoid modifying the original
    vcf_updated = vcf_table.copy()
    
    # Create a dictionary from remapping for faster lookup
    remap_dict = remapping.set_index('SNP_name').to_dict(orient='index')
    
    # Keep track of matched rows
    matched_indices = []
    
    # Update coordinates based on matching IDs
    for idx, row in vcf_updated.iterrows():
        if row['ID'] in remap_dict:
            vcf_updated.at[idx, 'CHROM'] = remap_dict[row['ID']]['new_genome_chromosome']
            vcf_updated.at[idx, 'POS'] = remap_dict[row['ID']]['new_genome_base_position']
            matched_indices.append(idx)
    
    # Keep only matched rows
    vcf_updated = vcf_updated.loc[matched_indices]
    
    # Convert POS column to object type first to allow mixed types
    vcf_updated['POS'] = vcf_updated['POS'].astype(object)
    
    # Convert numeric values to integers and keep '.' as is
    numeric_mask = pd.to_numeric(vcf_updated['POS'], errors='coerce').notna()
    vcf_updated.loc[numeric_mask, 'POS'] = pd.to_numeric(vcf_updated.loc[numeric_mask, 'POS']).astype(int)
    vcf_updated.loc[~numeric_mask, 'POS'] = '.'
    
    return vcf_updated

if SNP_remapping_path != None:
    VCF_table = update_vcf_coordinates(VCF_table, remapping)

def get_reference_base(row, genome_dict):
    chrom = row['CHROM']
    pos = row['POS'] 
    if chrom in genome_dict and pd.notna(pos) and pos != '.':
        try:
            pos = int(pos)
            # Adjust for 0-based indexing in Python
            return genome_dict[chrom].seq[pos - 1].upper()
        except ValueError:
            return '.'  # Return '.' if pos can't be converted to int
    return '.'  # Return '.' if chromosome not found or pos is invalid

# Populate the ref col, if not given
if assay_design_ref_allele_col == None:
    # Apply the function to populate the 'REF' column
    VCF_table['REF'] = VCF_table.apply(lambda row: get_reference_base(row, genome), axis=1)

genome = 0 # clear from memory

# Columns to iterate over (all except 'SNP')
trees = assay.columns[1:]

# Convert assay to dictionary only once
snp_dict = {snp: idx for idx, snp in enumerate(assay['SNP'])}

# Create result array
result = np.full((len(VCF_table), len(trees)), './.', dtype=object)

# Get indices for matching rows
vcf_indices = np.arange(len(VCF_table))
assay_indices = np.array([snp_dict.get(id_, -1) for id_ in VCF_table['ID']])

# Filter valid matches
valid_matches = assay_indices != -1
assay_indices = assay_indices[valid_matches]

# Use NumPy advanced indexing to fill the result
if valid_matches.any():
    result[valid_matches, :] = assay.iloc[assay_indices, 1:].to_numpy()

# Handle '--' values efficiently
result[result == '--'] = './.'

# Create a new dataframe with all columns at once and join with original
new_cols = pd.DataFrame(result, columns=trees, index=VCF_table.index)
VCF_table = pd.concat([VCF_table, new_cols], axis=1)

# FIND ALTERNATE ALLELES
def find_alternate_allele_with_freq(row, reference_allele):
    genotype_columns = row.iloc[9:]
    
    # Flatten the genotypes into a single string
    all_alleles = ''.join(genotype_columns.astype(str))
    
    # Count frequency of each allele
    allele_counts = {}
    for allele in all_alleles:
        if allele in ['A', 'C', 'T', 'G'] and allele != reference_allele:
            allele_counts[allele] = allele_counts.get(allele, 0) + 1
    
    if not allele_counts:
        return '.', {}
    
    # Sort alleles by frequency, descending
    sorted_alleles = sorted(allele_counts.items(), key=lambda x: x[1], reverse=True)
    alternate_alleles = ','.join(allele for allele, _ in sorted_alleles)
    
    return alternate_alleles, allele_counts

def merge_alt_alleles_ordered(existing_alt, found_alt, allele_freqs):
    # If existing_alt is empty or just a dot
    if pd.isna(existing_alt) or existing_alt == '.':
        return found_alt
    
    # Split existing alt into list
    existing_alleles = existing_alt.split(',')
    
    # Get the first allele (from assay_design)
    primary_alt = existing_alleles[0]
    
    # Get all other found alleles (excluding the primary_alt and reference)
    # and sort them by frequency
    other_alleles = []
    if found_alt != '.':
        other_alleles = [allele for allele in found_alt.split(',') 
                        if allele != primary_alt and allele != '.']
    
    # If there are no other alleles, return just the primary
    if not other_alleles:
        return primary_alt
    
    # Combine primary_alt with frequency-sorted other alleles
    return ','.join([primary_alt] + other_alleles)

if assay_design_alt_allele_col is not None:
    # First, set the initial ALT values from assay_design
    VCF_table['ALT'] = assay_design.iloc[:, assay_design_alt_allele_col]
    
    # Then find additional alternate alleles with their frequencies
    found_results = [find_alternate_allele_with_freq(row, row['REF']) 
                    for _, row in VCF_table.iterrows()]
    found_alt_alleles, allele_frequencies = zip(*found_results)
    
    # Merge while maintaining order
    VCF_table['ALT'] = [merge_alt_alleles_ordered(existing, found, freq) 
                        for existing, found, freq 
                        in zip(VCF_table['ALT'], found_alt_alleles, allele_frequencies)]
if assay_design_alt_allele_col == None:
    # If no assay_design_alt_allele_col, just use frequency-sorted alleles
    found_results = [find_alternate_allele_with_freq(row, row['REF']) 
                    for _, row in VCF_table.iterrows()]
    VCF_table['ALT'], _ = zip(*found_results)

# CHANGE SNPS FROM FORMAT E.G. "A/G" TO FORMAT "0/1"
def transform_genotypes_fast(VCF_table):
    """
    Transform genotypes from base format (e.g., 'AG') to VCF format (e.g., '0/1')
    using vectorized operations for faster execution.
    """
    ref = VCF_table['REF'].values
    alt = VCF_table['ALT'].str.split(',').values
    
    # Create a copy to avoid modifying the original
    result_table = VCF_table.copy()
    
    # Process each genotype column in bulk
    for col in VCF_table.columns[9:]:
        genotypes = VCF_table[col].values

        # Vectorized transformation
        transformed = np.full_like(genotypes, './.', dtype=object)
        
        # Mask for valid genotypes
        valid_mask = (~pd.isna(genotypes)) & (genotypes != '.') & (genotypes != './.') & (genotypes != '--')
        
        if not valid_mask.any():
            continue
        
        valid_genotypes = genotypes[valid_mask]
        valid_refs = ref[valid_mask]
        valid_alts = alt[valid_mask]
        
        # Transform valid genotypes
        def transform_single_genotype(index):
            genotype = valid_genotypes[index]
            ref_allele = valid_refs[index]
            alt_alleles = valid_alts[index]
            
            if not isinstance(genotype, str) or not isinstance(alt_alleles, list):
                return './.'
            
            allele1, allele2 = genotype[0], genotype[1] if len(genotype) > 1 else '.'
            
            # Transform alleles
            def transform_allele(allele):
                if allele == '.':
                    return '.'
                elif allele == ref_allele:
                    return '0'
                try:
                    return str(alt_alleles.index(allele) + 1)
                except ValueError:
                    return '.'
            
            transformed_allele1 = transform_allele(allele1)
            transformed_allele2 = transform_allele(allele2)
            
            return f"{transformed_allele1}/{transformed_allele2}"
        
        # Apply the transformation to valid genotypes
        transformed_genotypes = np.array([transform_single_genotype(i) for i in range(len(valid_genotypes))], dtype=object)
        
        # Update transformed results back into the column
        transformed[valid_mask] = transformed_genotypes
        result_table[col] = transformed
    
    return result_table

VCF_table = transform_genotypes_fast(VCF_table)

# Remove rows with no genotype calls
VCF_table = VCF_table.loc[~(VCF_table.iloc[:, 9:].eq("./.").all(axis=1))]

def md5(sequence):
    return hashlib.md5(sequence.encode()).hexdigest()

# Get unique contig IDs
contig_IDs = [contig for contig in VCF_table['CHROM'].unique() if not pd.isna(contig)]

# Calculate assembly length and get individual contig lengths and MD5 checksums
assembly_length = 0
contig_lengths = {}
contig_md5s = {}
fasta_ids = set()

for record in SeqIO.parse(fasta_path, "fasta"):
    fasta_ids.add(record.id)
    contig_lengths[record.id] = len(record.seq)
    contig_md5s[record.id] = md5(str(record.seq))
    assembly_length += len(record.seq)

# Generate VCF header
vcf_header = [
    '##fileformat=VCFv4.3',
    f'##fileDate={datetime.now().strftime("%Y%m%d")}',
    '##source=MylesVCFCreationProgram',
]

# Add separate contig lines for each contig ID
for contig_id in contig_IDs:
    # Convert contig_id to string before comparison
    contig_id_str = str(contig_id)
    
    # Try to find a matching FASTA ID
    matching_fasta_id = next((fasta_id for fasta_id in fasta_ids if contig_id_str in fasta_id), None)
    
    if matching_fasta_id:
        vcf_header.append(f'##contig=<ID={contig_id},length={contig_lengths[matching_fasta_id]},assembly={assembly_version},md5={contig_md5s[matching_fasta_id]},species="{species}">')
    else:
        vcf_header.append(f'##contig=<ID={contig_id},length=unknown,assembly={assembly_version},md5=unknown,species="{species}">')

# Add remaining header lines
if fasta_online_path != "" and fasta_online_path != None:
    vcf_header.extend([f'##reference=file://{fasta_online_path}'])

vcf_header.extend(['##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">'])

# GENERATE AND EXPORT FINAL VCF
# Mock VCF data
vcf_rows = VCF_table.apply(lambda row: "\t".join(row.astype(str)), axis=1)
format_fields = "\t".join(trees)
header_line = "\t".join(["#CHROM", "POS", "ID", "REF", "ALT", "QUAL", "FILTER", "INFO", "FORMAT", format_fields])
vcf_content = "\n".join([*vcf_header, header_line, *vcf_rows])

# Define the output file path in the current directory
script_dir = os.getcwd()

output_file_name = f"{study_info}.{assembly_version}.vcf"
output_path = os.path.join(script_dir, output_file_name)

with open(output_path, "w") as output_file:
    output_file.write(vcf_content)