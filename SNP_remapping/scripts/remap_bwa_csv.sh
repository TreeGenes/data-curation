#!/bin/bash
#SBATCH --job-name=SNP_probe_remapping
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=15G
#SBATCH --qos=general
#SBATCH --error=SNP_probe_remapping-%j.err

##############################################################  USER INPUT  #######################################################################
# Input parameters
NEW_GENOME_VERSION="v4"                    # v{integer}
SPECIES="Populus_trichocarpa"              # {Genus}_{species}
TGDR_NUMBER="665"                                 # {integer}

# File paths
NEW_GENOME="/core/labs/Wegrzyn/PopTrich_2024/PopTrich_2024/genomes/v4.1/Ptrichocarpa_533_v4.0.fa"          # path to new genome to map to
SNP_PROBES="/core/labs/Wegrzyn/PopTrich_2024/PopTrich_2024/studies/Geraldes_2013/manifest_chip34K.csv"      # path to SNPs that will be remapped

# manifest chip / flank data colname assignments (from a table with dropdown headers in TPPS) (0-indexed)
SNP_NAME_COL=0      # column containing original name of snp
SNP_CHR_COL=4       # column containing original chrom number of snp
SNP_POS_COL=5       # column containing original base position of snp
SNP_SEQ_COL=9       # column containing original flank sequence
####################################################################################################################################################

# Generate output filename based on input parameters
OUTPUT_FILE="${TGDR_NUMBER}.FlanksMappedTo.${SPECIES}.${NEW_GENOME_VERSION}.csv"

# Basic error handling setup
set -e              # Exit on any error
set -o pipefail     # Pipe operations fail on any component failure

# Set number of threads for parallel processing
THREADS=36

# Load required modules
module load bwa
module load samtools
module load bedtools

# Set up temporary directory - prefer RAM disk if available
if [[ -d "/dev/shm" && -w "/dev/shm" ]]; then
    TMPDIR="/dev/shm/$USER/snp_remap_$$"    # Use RAM disk for better performance
else
    TMPDIR="/tmp/$USER/snp_remap_$$"        # Fallback to regular tmp
fi
mkdir -p "$TMPDIR"

# Define temporary file paths
TEMP_BAM="$TMPDIR/aligned_snps.bam"
TEMP_SORTED_BAM="$TMPDIR/aligned_snps_sorted.bam"
TEMP_BED="$TMPDIR/aligned_snps.bed"
TEMP_FASTA="$TMPDIR/temp_snps.fasta"
TEMP_REGIONS="$TMPDIR/temp_regions.txt"
TEMP_REMAPPED="$TMPDIR/temp_remapped.fa"

# Cleanup function to remove temporary files
cleanup() {
    rm -rf "$TMPDIR"
}
trap cleanup EXIT    # Ensure cleanup runs even if script fails

# Helper function to verify input files exist and are readable
check_file() {
    if [[ ! -r "$1" ]]; then
        echo "Error: File $1 does not exist or is not readable." >&2
        exit 1
    fi
}

# Validate input files
check_file "$NEW_GENOME"
check_file "$SNP_PROBES"

# Create genome indices if they don't exist
if [[ ! -f "${NEW_GENOME}.fai" ]]; then
    echo "Indexing new genome..."
    samtools faidx "$NEW_GENOME"
fi
if [[ ! -f "${NEW_GENOME}.bwt" ]]; then
    echo "Creating BWA index for new genome..."
    bwa index "$NEW_GENOME"
fi

# Process SNP sequences into FASTA format
echo "Creating FASTA file with all SNP sequences..."
# Convert CSV to FASTA, replacing SNP markers with N
awk -F',' '
BEGIN {
    OFS=",";
    PROCINFO["sorted_in"] = "@ind_num_asc"
}
NR>1 {
    name=$1;
    seq=$10;
    gsub(/\[.*?\]/, "N", seq);    # Replace SNP markers with N
    print ">" name "\n" seq
}' "$SNP_PROBES" | split -l 10000 - "$TMPDIR/split_snps_"

# Combine split files in parallel
for split_file in "$TMPDIR"/split_snps_*; do
    cat "$split_file" >> "$TEMP_FASTA" &
done
wait

# Align sequences and convert to sorted BAM in a single pipeline
echo "Aligning sequences and converting to sorted BAM..."
bwa mem -t "$THREADS" -K 100000000 -Y "$NEW_GENOME" "$TEMP_FASTA" | \
    samtools view -@ "$THREADS" -bS -q 30 - | \
    samtools sort -@ "$THREADS" -m 2G -T "$TMPDIR/temp_sort" -o "$TEMP_SORTED_BAM"
samtools index -@ "$THREADS" "$TEMP_SORTED_BAM"

# Convert to BED format for easier processing
echo "Converting BAM to BED..."
bedtools bamtobed -i "$TEMP_SORTED_BAM" > "$TEMP_BED"

# Clean up intermediate file
rm -f "$TEMP_REMAPPED"

# Process alignments in batches for better performance
split -l 1000 "$TEMP_BED" "$TMPDIR/bed_split_"
for split_file in "$TMPDIR"/bed_split_*; do
    (
        while read -r line; do
            chr=$(echo "$line" | cut -f1)
            start=$(echo "$line" | cut -f2)
            end=$(echo "$line" | cut -f3)
            name=$(echo "$line" | cut -f4)
            
            # Extract sequence from new genome
            if samtools faidx "$NEW_GENOME" "${chr}:${start}-${end}" 2>/dev/null; then
                samtools faidx "$NEW_GENOME" "${chr}:${start}-${end}" | \
                awk -v name="$name" 'NR==1{print ">" name} NR>1'
            else
                echo ">$name"
                echo "N"
            fi
        done < "$split_file" >> "$TEMP_REMAPPED.${split_file##*/}"
    ) &
    
    # Limit parallel processes
    if [[ $(jobs -p | wc -l) -ge $THREADS ]]; then
        wait -n
    fi
done
wait

# Combine processed files
cat "$TEMP_REMAPPED".* > "$TEMP_REMAPPED"

# Generate final output file
echo "Processing alignments and creating output..."
echo "SNP_name,old_genome_chromosome,old_genome_base_position,new_genome_chromosome,new_genome_base_position,remapping_quality,times_mapped,original_sequence,new_sequence" > "$OUTPUT_FILE"

# Process alignments and create final output
# This awk script handles:
# - Matching original and remapped sequences
# - Handling reverse complement sequences
# - Calculating new SNP positions
# - Formatting output data
awk -F'[,\t]' -v snp_name_col="$SNP_NAME_COL" -v snp_chr_col="$SNP_CHR_COL" -v snp_pos_col="$SNP_POS_COL" -v snp_seq_col="$SNP_SEQ_COL" '
BEGIN {
    OFS=",";
    PROCINFO["sorted_in"] = "@ind_num_asc"
}
# Read remapped sequences
FILENAME == ARGV[1] {
    if ($0 ~ /^>/) {
        current_name = substr($0, 2)
        next
    }
    remapped_seq[current_name] = remapped_seq[current_name] $0
    next
}
# Process BED file alignments
FILENAME == ARGV[2] {
    if (!($4 in chr)) {
        chr[$4] = $1
        pos[$4] = $2
        end[$4] = $3
        strand[$4] = $6
        qual[$4] = $5
        count[$4] = 1
    } else {
        count[$4]++
    }
    next
}
# Process original SNP data and generate output
FNR>1 {
    name=$snp_name_col
    old_chr=$snp_chr_col
    old_pos=$snp_pos_col
    original_seq=$snp_seq_col
    gsub(/\[.*?\]/, "N", original_seq)

    if (name in chr && count[name] == 1) {
        new_seq = remapped_seq[name]
        if (strand[name] == "-") {
            cmd = "echo \"" new_seq "\" | rev | tr \"ACGTacgt\" \"TGCAtgca\""
            cmd | getline new_seq
            close(cmd)
        }

        gsub(/\r/, "", original_seq)
        gsub(/\n/, "", original_seq)
        gsub(/\r/, "", new_seq)
        gsub(/\n/, "", new_seq)

        match($snp_seq_col, /\[.*?\]/)
        snp_probe_length = RSTART - 1
        new_snp_position = (strand[name] == "+") ? \
            pos[name] + snp_probe_length + 1 : \
            end[name] - snp_probe_length - 1

        print name, old_chr, old_pos, chr[name], new_snp_position, qual[name], original_seq, new_seq
    } else {
        print name, old_chr, old_pos, "", "", "", count[name], original_seq, ""
    }
}
' "$TEMP_REMAPPED" "$TEMP_BED" "$SNP_PROBES" >> "$OUTPUT_FILE"

# Clean up the output file (remove empty lines and trailing commas)
awk 'BEGIN {
    FS=OFS=",";
    RS="\r\n|\n|\r"
}
NF > 1 || $0 !~ /^,*$/ {
    gsub(/^,|,$/, "")
    if (NF > 0) print
}' "$OUTPUT_FILE" > "${OUTPUT_FILE}.tmp" && mv "${OUTPUT_FILE}.tmp" "$OUTPUT_FILE"

echo "Processing complete. Results saved in $OUTPUT_FILE."

# Clean up error files if everything worked
if [ $? -eq 0 ]; then
    rm "SNP_probe_remapping-${SLURM_JOB_ID}.err"
    rm "slurm-${SLURM_JOB_ID}.out"
fi