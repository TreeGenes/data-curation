#################################################################### USER INPUT ###########################################################
# INPUT NEEDED FOR ALL OPTIONS
fasta_path = "/Users/meghanmyles/Desktop/Populus_trichocarpa/genomes/v3/Potr.3_0.fa" 
vcf_path = "/Users/meghanmyles/Desktop/Populus_trichocarpa/studies/Geraldes_2013/Geraldes_2013.v2.vcf"
SNP_remapping_path = "/Users/meghanmyles/Desktop/Populus_trichocarpa/bwa_remapped_csvs/Geraldes_2013.FlanksMappedTo.Populus_trichocarpa.v3.csv"  

fasta_online_path = ""   # should link to our FTP site (correct directory)
species = "Populus trichocarpa"
study_info = "Geraldes_2013"
assembly_version = "v3"
############################################################################################################################################

#############################################################  OUTPUT  #####################################################################
# a VCF of the SNPs mapped to a new genome version
############################################################################################################################################



# LIBRARIES
import pandas as pd
import numpy as np
from Bio import SeqIO
from io import StringIO
from datetime import datetime
import hashlib
import os
import dask.dataframe as dd

SNP_remapping = pd.read_csv(SNP_remapping_path, low_memory = False)

vcf_headers = []
table_start = False
table_data = []

# Open and read the VCF file line-by-line
with open(vcf_path, 'r') as file:
    for line in file:
        # Check if the line starts with '##' (single-cell headers in VCF)
        if line.startswith('##'):
            vcf_headers.append(line.strip())
        # Detect the beginning of the main table (starts with '#CHROM')
        elif line.startswith('#CHROM'):
            table_data.append(line.strip())
            table_start = True
        # Collect the rest of the lines as table data
        elif table_start:
            table_data.append(line.strip())

# Join table_data list into a single string with line breaks, then read as CSV
vcf_table = pd.read_csv(StringIO("\n".join(table_data)), sep='\t')

len_vcf_table = len(vcf_table)

vcf_table['QUAL'] = ['.'] * len_vcf_table
vcf_table['FILTER'] = ['.'] * len_vcf_table
vcf_table['INFO'] = ['.'] * len_vcf_table
vcf_table['FORMAT'] = ['GT'] * len_vcf_table

def remap_vcf_coordinates(vcf_table, snp_remapping):
    """
    Remap VCF coordinates using SNP remapping information while properly handling data types.
    
    Parameters:
    vcf_table (pandas.DataFrame): Original VCF table
    snp_remapping (pandas.DataFrame): SNP remapping information
    
    Returns:
    pandas.DataFrame: Updated VCF table with remapped coordinates
    """
    # Create a copy of the vcf_table to avoid modifying the original
    vcf_updated = vcf_table.copy()
    
    # Ensure #CHROM column is string type from the start
    vcf_updated['#CHROM'] = vcf_updated['#CHROM'].astype(str)
    
    # Create a mapping dictionary from SNP_remapping
    # Convert chromosome and position to appropriate types during dictionary creation
    remapping_dict = dict(zip(
        snp_remapping['SNP_name'],
        zip(
            snp_remapping['new_genome_chromosome'].astype(str),
            snp_remapping['new_genome_base_position'].astype(float)
        )
    ))
    
    # Create lists to hold updated values
    new_chroms = []
    new_positions = []
    
    # Update CHROM and POS based on the mapping using the ID column
    for idx, row in vcf_updated.iterrows():
        if row['ID'] in remapping_dict:
            new_chrom, new_pos = remapping_dict[row['ID']]
            if pd.notna(new_chrom) and pd.notna(new_pos):
                new_chroms.append(str(new_chrom))
                new_positions.append(int(new_pos))
            else:
                new_chroms.append(row['#CHROM'])
                new_positions.append(row['POS'])
        else:
            new_chroms.append(row['#CHROM'])
            new_positions.append(row['POS'])
    
    # Bulk update the DataFrame
    vcf_updated['#CHROM'] = new_chroms
    vcf_updated['POS'] = new_positions
    
    # Sort by chromosome and position
    # Extract chromosome number for proper sorting
    vcf_updated['sort_key'] = vcf_updated['#CHROM'].str.extract(r'(\d+)').astype(float)
    vcf_updated = vcf_updated.sort_values(['sort_key', 'POS'])
    vcf_updated = vcf_updated.drop('sort_key', axis=1)
    
    return vcf_updated

# Apply the remapping
vcf_table = remap_vcf_coordinates(vcf_table, SNP_remapping)

def reverse_transform_genotypes_optimized(vcf_table):
    """
    Convert genotypes from numerical format (e.g., "0/1") to nucleotide format (e.g., "G/A"),
    while preserving all other columns exactly as they are.
    
    Parameters:
    vcf_table (pandas.DataFrame): VCF table with REF, ALT columns and genotype columns
    
    Returns:
    pandas.DataFrame: VCF table with transformed genotype columns
    """
    # Create a copy to avoid modifying the original
    result = vcf_table.copy()
    
    # Get genotype columns (columns after position 8)
    genotype_cols = vcf_table.columns[9:]
    
    if len(genotype_cols) == 0:
        return result
        
    # Create a lookup function that handles the conversion
    def transform_genotype(row, gt_col):
        genotype = row[gt_col]
        ref = row['REF']
        alt = row['ALT']
        
        if not isinstance(genotype, str) or genotype == "./." or "/" not in genotype:
            return genotype
            
        allele1, allele2 = genotype.split("/")
        alt_alleles = alt.split(",") if isinstance(alt, str) else []
        
        # Vectorized allele transformation using numpy arrays
        def transform_allele(allele):
            if allele == ".":
                return "."
            if allele == "0":
                return ref
            try:
                allele_idx = int(allele)
                if allele_idx <= len(alt_alleles):
                    return alt_alleles[allele_idx-1]
            except ValueError:
                pass
            return "."
            
        return f"{transform_allele(allele1)}/{transform_allele(allele2)}"
    
    # Apply the transformation to each genotype column using pandas apply
    for col in genotype_cols:
        result[col] = result.apply(lambda row: transform_genotype(row, col), axis=1)
        
    return result
        
# Create a lookup function that handles the conversion
def reverse_transform_genotypes_parallel(vcf_table, n_jobs=-1):
    """
    Parallel version of the genotype transformation using Dask for large datasets.
    
    Parameters:
    vcf_table (pandas.DataFrame): VCF table with REF, ALT columns and genotype columns
    n_jobs (int): Number of parallel jobs. -1 means use all available cores.
    
    Returns:
    pandas.DataFrame: VCF table with transformed genotype columns
    """        
    # Create metadata for the output DataFrame
    meta = vcf_table.iloc[:0].copy()  # Empty DataFrame with same structure
    
    # Convert to Dask DataFrame with proper metadata
    ddf = dd.from_pandas(vcf_table, npartitions=abs(n_jobs))
    
    # Get genotype columns
    genotype_cols = vcf_table.columns[9:]
    
    if len(genotype_cols) == 0:
        return vcf_table
        
    # Create a lookup function that handles the conversion
    def transform_genotype(row, gt_col):
        genotype = row[gt_col]
        ref = row['REF']
        alt = row['ALT']
        
        if not isinstance(genotype, str) or genotype == "./." or "/" not in genotype:
            return genotype
            
        allele1, allele2 = genotype.split("/")
        alt_alleles = alt.split(",") if isinstance(alt, str) else []
        
        def transform_allele(allele):
            if allele == ".":
                return "."
            if allele == "0":
                return ref
            try:
                allele_idx = int(allele)
                if allele_idx <= len(alt_alleles):
                    return alt_alleles[allele_idx-1]
            except ValueError:
                pass
            return "."
            
        return f"{transform_allele(allele1)}/{transform_allele(allele2)}"
    
    # Apply transformation to each genotype column with proper metadata
    result = ddf.copy()
    for col in genotype_cols:
        result[col] = ddf.map_partitions(
            lambda df: df.apply(
                lambda row: transform_genotype(row, col), 
                axis=1
            ),
            meta=(col, 'object')
        )
    
    # Compute and return result
    return result.compute()

# Apply the reverse transformation to the VCF table
vcf_table = reverse_transform_genotypes_parallel(vcf_table)

vcf_table['REF'] = ['.'] * len_vcf_table
vcf_table['ALT'] = ['.'] * len_vcf_table

# ADD REFERENCE ALLELES TO THE MOCK VCF
# Read the entire fasta file into memory
new_genome = SeqIO.to_dict(SeqIO.parse(fasta_path, "fasta"))

# CHECKS TO ENSURE THAT THE MAJORITY OF CHR NAMES REFERENCED WITHIN THE VCF EXIST WITHIN THE FASTA
vcf_table['#CHROM'] = vcf_table['#CHROM'].fillna('.')
vcf_table['POS'] = vcf_table['POS'].fillna('.')

def get_reference_base(row, genome_dict):
    chrom = row['#CHROM']
    pos = row['POS'] 
    if chrom in genome_dict and pd.notna(pos) and pos != '.':
        try:
            pos = int(pos)
            # Adjust for 0-based indexing in Python
            return genome_dict[chrom].seq[pos - 1].upper()
        except ValueError:
            return '.'  # Return '.' if pos can't be converted to int
    return '.'  # Return '.' if chromosome not found or pos is invalid

# Apply the function to populate the 'REF' column
vcf_table['POS'] = pd.to_numeric(vcf_table['POS'], errors='coerce')
vcf_table['REF'] = vcf_table.apply(lambda row: get_reference_base(row, new_genome), axis=1)

new_genome = 0 # clear from memory

# FIND ALTERNATE ALLELES
def find_alternate_allele(row, reference_allele):
    genotype_columns = row.iloc[9:]
    
    # Flatten the genotypes into a single string
    all_alleles = ''.join(genotype_columns.astype(str))
    
    # Create a dictionary to store allele frequencies
    allele_freq = {}
    
    # Count frequency of each allele
    for allele in all_alleles:
        if allele.isalpha():  # Only count alphabetic characters
            if allele != reference_allele:  # Don't count reference allele
                allele_freq[allele] = allele_freq.get(allele, 0) + 1
    
    if not allele_freq:
        return '.'  # Handle cases with monomorphic loci
    
    # Sort alleles by frequency in descending order
    sorted_alleles = sorted(allele_freq.items(), key=lambda x: (-x[1], x[0]))
    # Convert to comma-separated string of just the alleles (not their frequencies)
    alternate_alleles = ','.join(allele[0] for allele in sorted_alleles)
    
    return alternate_alleles

vcf_table['ALT'] = vcf_table.apply(lambda row: find_alternate_allele(row, row['REF']), axis=1)

# CHANGE SNPS FROM FORMAT E.G. "A/G" TO FORMAT "0/1"
def transform_genotypes_fast(VCF_table):
    ref = VCF_table['REF'].values
    alt = VCF_table['ALT'].str.split(",").values
    
    for j in range(9, len(VCF_table.columns)):
        genotypes = VCF_table.iloc[:, j]
        for i in range(len(VCF_table)):
            genotype = genotypes.iat[i]
            if isinstance(genotype, str) and "/" in genotype:
                allele1, allele2 = genotype.split("/")
                
                transformed_allele1 = "0" if allele1 == ref[i] else str(alt[i].index(allele1) + 1) if allele1 in alt[i] else "."
                transformed_allele2 = "0" if allele2 == ref[i] else str(alt[i].index(allele2) + 1) if allele2 in alt[i] else "."
                
                # Update the VCF_table with transformed genotype
                VCF_table.iat[i, j] = f"{transformed_allele1}/{transformed_allele2}"
            elif genotype == ".":
                VCF_table.iat[i, j] = "./."  # Set missing data to "./."
    
    return VCF_table


# Apply the corrected transform_genotypes_fast function to the VCF_table
vcf_table = transform_genotypes_fast(vcf_table)

def md5(sequence):
    return hashlib.md5(sequence.encode()).hexdigest()

# Get unique contig IDs
contig_IDs = [contig for contig in vcf_table['#CHROM'].unique() if not pd.isna(contig)]

# Calculate assembly length and get individual contig lengths and MD5 checksums
assembly_length = 0
contig_lengths = {}
contig_md5s = {}
fasta_ids = set()

for record in SeqIO.parse(fasta_path, "fasta"):
    fasta_ids.add(record.id)
    contig_lengths[record.id] = len(record.seq)
    contig_md5s[record.id] = md5(str(record.seq))
    assembly_length += len(record.seq)

# Generate VCF header
vcf_header = [
    '##fileformat=VCFv4.3',
    f'##fileDate={datetime.now().strftime("%Y%m%d")}',
    '##source=MylesVCFCreationProgram',
]

# Add separate contig lines for each contig ID
for contig_id in contig_IDs:
    # Try to find a matching FASTA ID
    matching_fasta_id = next((fasta_id for fasta_id in fasta_ids if contig_id in fasta_id), None)
    
    if matching_fasta_id:
        vcf_header.append(f'##contig=<ID={contig_id},length={contig_lengths[matching_fasta_id]},assembly={assembly_version},md5={contig_md5s[matching_fasta_id]},species="{species}">')
    else:
        vcf_header.append(f'##contig=<ID={contig_id},length=unknown,assembly={assembly_version},md5=unknown,species="{species}">')

# Add remaining header lines
if fasta_online_path != "":
    vcf_header.extend([f'##reference=file://{fasta_online_path}'])

vcf_header.extend(['##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">'])

# GENERATE AND EXPORT FINAL VCF
# Mock VCF data
vcf_rows = vcf_table.apply(lambda row: "\t".join(row.astype(str)), axis=1)
header_line = "\t".join(vcf_table.columns)
vcf_content = "\n".join([*vcf_header, header_line, *vcf_rows])

# Get absolute path of current directory
current_dir = os.path.abspath(os.path.dirname(__file__))
output_file_name = f"{study_info}.{assembly_version}.vcf"
output_path = current_dir + '/' + output_file_name

# Export VCF
with open(output_path, "w") as output_file:
    output_file.write(vcf_content)