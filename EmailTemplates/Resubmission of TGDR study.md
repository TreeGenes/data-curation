**Dear Dr. X**

Hello, my name is **insert name** and I am an undergraduate research assistant for the Plant Computational Genomics Laboratory at the University of Connecticut. My main goal here is to curate scientific studies (population studies on a variety of plant species) with data that could be uploaded to our database, treegenesdb.org.

TreeGenes is a plant genomic database that holds genetic, phenotypic, and location information for plants across the world. These data are integrated, displayed, and analyzed in our interactive web-based application, CartograPlant (http://treegenesdb.org/cartogratree).

I am emailing in regards to your **(YEAR)** paper titled, **(TITLE)**, but I need some additional information to finish the resubmission!

We previously held data from this study in TreeGenes, but when attempting to resubmit the study through the new TPPS pipeline, we ...

Once this data is loaded into the system and viewable in CartograPlant, I will provide a link for you to access and share this information.  Please note that we also can assist in accepting publications and supporting data at the time of submission via the Tripal Plant PopGen Submission pipeline available here: https://treegenesdb.org/tpps . 

To become a registered member of the TreeGenes colleague directory and database, you can obtain an account here: https://treegenesdb.org/user/registermulti/step1 .

Thank you in advance for your assistance!

**insert name**
