**Dr. X:**

My name is  **insert name** and I am an undergraduate research assistant in the Plant Computational Genomics Laboratory at the University of Connecticut. I am curating published studies (population studies on plant species) to extract relevant data that can be uploaded to the TreeGenes database, treegenesdb.org.

Treegenes is a web-based genomic and phenomic repository that holds genetic, phenotypic, and environmental data on georeferenced population studies on a variety of plant species, that can eventually be integrated, displayed, and analyzed in our interactive web application, CartograPlant (http://treegenesdb.org/cartogratree).

I am e-mailing in regards to your **(YEAR)** study, ***Study title***, to let you know that the study has been successfully uploaded to our database. The linked data are viewable at https://treegenesdb.org/tpps/details/ **(ACCESSION)** .

The final accession number for your study is **(ACCESSION)**. This is the TreeGenes accession number that you can use to view it on CartograPlant through https://treegenesdb.org/cartogratree?accession=TGDR **(ACCESSION)**  .You can also use this ID or request a DOI from us to use in future publications.  Please note that while we do our best to extract data accurately from supplemental files, Dryad, and other sources, there may be small errors introduced and we would appreciate a brief review of this submission.  If you would like to provide additional data or modifications, please do not hesitate to contact me or treegenesdb@gmail.com

Please note that we also can assist in accepting publications and supporting data at the time of submission via the Plant PopGen Submission pipeline available here: https://treegenesdb.org/tpps . 

To become a registered member of the TreeGenes colleague directory and database, you can obtain an account here: https://treegenesdb.org/user/registermulti/step1 .

Thanks,

** insert name again **
