<h3> Data Curation: <h3>

<h4> Treegenes Data Curation

*  [TPPSc Tutorial](https://gitlab.com/TreeGenes/data-curation/blob/master/TPPSc%20Tutorial.md): This links to our TPPSc tutorial, which outlines everything a user needs to make and upload any type of TPPSc Submissions.
*  [Study Loads SpreadSheet](https://docs.google.com/spreadsheets/d/1ZMcREgdogpOPbW_aWdD2TL7QkdszAdqSjwNBH-DRXzA/edit#gid=1771003511): This spreadsheet contains all studies previously reviewed or currently under review, along with notes.
*  [Species Image Curation](https://gitlab.com/TreeGenes/data-curation/-/blob/master/Species%20Image%20Curation.md): Explains Protocol for large scale curation of species images
*  [TPPSc](https://treegenesdb.org/tppsc): This links to the internal pipeline that is used to submit the genetic, phenotypic, and environmental data to our database [TreeGenes](treegenesdb.org)
*  [Email Templates](https://gitlab.com/TreeGenes/data-curation/blob/master/EmailTemplates): Links to the email templates that would be sent out to the authors of the study, either requesting further information or informing them of the uploaded study
*  [CartograTree](https://treegenesdb.org/ct): The end goal of submitting new trees and new data to the database, is to be able to view them in our mapview CartograTree, that also links directly back to the scientific data uploaded with the trees.


